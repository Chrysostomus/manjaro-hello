<big>Manjaro 20.2</big>

Siamo felici di pubblicare un'altra versione stabile di Manjaro Linux, denominata Nibia.

<big>L'edizione Gnome</big> è l'epicentro di nuove interessanti funzionalità. Ha ricevuto una profonda revisione, forse il più grande aggiornamento fino ad ora.

Gnome 3.38 fornisce miglioramenti delle prestazioni, griglia dell'applicazione notevolmente migliorata, controllo genitoriale, nuovo tour di benvenuto e molte altre funzionalità. La nuova installazione in stile OEM rende il processo di installazione estremamente semplice e Gnome-Initial-Setup ti consente di installare facilmente Manjaro per altre persone.

Anche l'Application-Utility di Manjaro ha ottenuto molti miglioramenti, consentendo di scegliere facilmente i browser, le suite per ufficio e i gestori di password preferiti. Abbiamo incluso anche due eccellenti tiling manager delle finestre: l'intuitiva Pop-shell e la Material-shell touch friendly. Questi possono essere abilitati in Gnome-Layout-Switcher.

Anche le scorciatoie da tastiera sono state migliorate per gestire i desktop virtuali in modo più efficiente. La modalità scura automatica ha ottenuto controlli più granulari e ti consente anche di cambiare automaticamente il tema delle applicazioni qt. L'edizione di Gnome ora include anche una schermata iniziale e finestre di dialogo grafiche per la password per i sistemi crittografati. Abbiamo anche migliorato il desktop in modo che ora utilizzi circa il 40% di RAM in meno rispetto a prima. Ora utilizziamo automaticamente Wayland anche su hardware non Nvidia.

La nostra <big>edizione KDE</big> fornisce l'ambiente desktop Plasma 5.20 potente, maturo e ricco di funzionalità con un aspetto unico. La nuova versione presenta miglioramenti Wayland, vassoio di sistema simile a una griglia, nuove opzioni di risparmio energetico, centro impostazioni migliorato, supporto touch in dolphin e molto altro. Con un'ampia selezione delle ultime app KDE 20.08 e altre applicazioni, Manjaro-KDE mira ad essere un ambiente versatile ed elegante pronto per tutte le tue esigenze quotidiane.

Con la nostra <big>edizione XFCE</big>, solo pochi possono affermare di offrire un'esperienza Xfce così raffinata, integrata e all'avanguardia. Con questa versione viene distribuito Xfce 4.14. La filosofia Xfce si accoppia bene con il rilascio rolling all'avanguardia che è Manjaro, offrendo prestazioni affidabili senza sorprese.

Per questa versione viene utilizzato il kernel 5.9, come i driver più recenti disponibili fino ad oggi. Con il kernel 5.4-LTS iso minimpo offriamo supporto aggiuntivo per hardware meno recente.

Ultimo, ma non meno importante, anche il nostro installatore Calamares ha ricevuto molti miglioramenti. Tra le altre cose, ora supporta sistemi crittografati senza partizione crittografata/di avvio. Ciò abilita le finestre di dialogo grafiche delle password, utilizzando mappe di chiavi non statunitensi per l'immissione delle password e tempi di avvio fino a 1 minuto più brevi rispetto alla crittografia completa del disco. Il partizionamento automatico utilizza ancora la crittografia completa del disco per impostazione predefinita.

Ci auguriamo che questa versione ti piaccia e facci sapere cosa ne pensi di Nibia.